# SAS to CSV exporter

**Prerequisites:** SAS, Python

## Instructions for batch conversion
1. Make sure SAS is on the path
2. Put the files you want to convert in the data-sas folder
3. Run the Python script convert_all_files.py
4. Find the exported CSV files in the data-csv folder

## Instructions for single file conversion
Make sure SAS is on the path and then run:

```shell
sas sas_to_csv.sas ^
    -set in "<absolute_path>/<filename>.sas7bdat" ^
    -set out "<absolute_path>/<filename>.csv"
```
