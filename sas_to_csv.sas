*******************************************************************************
* A tool for converting SAS data files (sas7bdat) to CSV files
* Author: Peter Alping
*
* Add SAS to path, e.g. C:\Program Files\SASHome\SASFoundation\9.4
*
* Run with:

sas sas_to_csv.sas ^
  -set in "<absolute_path>/<filename>.sas7bdat" ^
  -set out "<absolute_path>/<filename>.csv.gz"

******************************************************************************;

* Allow spaces in SAS filenames;
OPTIONS VALIDMEMNAME=EXTEND;
* Allow any variable name;
OPTIONS VALIDVARNAME=ANY;
* Set missing indicator to empty string;
OPTIONS MISSING="";

* Get command line arguments and make sure to use Window's slashes;
%LET in_file =  %SYSFUNC(TRANSLATE(%SYSGET(in),"\","/"));
%LET out_file =  %SYSFUNC(TRANSLATE(%SYSGET(out),"\","/"));

* Get lib_path and sas_file from in_file;
%LET lib_path = %SUBSTR(&in_file, 1, %LENGTH(&in_file) - %LENGTH(%SCAN(&in_file, -1, "\")));
%LET sas_file = %SCAN(%SCAN(&in_file, -1, "\"), 1, ".");

* Get csv_file and csv_file_ext (csv or gz) from out_file;
%LET csv_file = %SCAN(%SCAN(&out_file, -1, "\"), 1, ".");
%LET csv_file_ext = %SCAN(%SCAN(&out_file, -1, "\"), -1, ".");

* Set libname;
LIBNAME data_lib "&lib_path" ACCESS=READONLY;

* Get the dataset metadata;
PROC CONTENTS DATA = data_lib."&sas_file"n OUT = contents NOPRINT;
RUN;

* Create temp dataset to modify;
DATA tmp_data;
   SET data_lib."&sas_file"n;
RUN;

* Get a list of all date variables;
* Initialize list to handle no date variables;
%LET date_vars = "";
PROC SQL NOPRINT;
	SELECT NAME
	INTO: date_vars SEPARATED BY " "
	FROM contents
	WHERE FORMAT = "DATE";
QUIT;

* If there are date variables, convert them to proper date format;
%IF &date_vars NE "" %THEN %DO;
	DATA tmp_data;
		SET tmp_data;
		FORMAT &date_vars YYMMDD10.;
	RUN;
%END;

* Get a list of all datetime variables;
* Initialize list to handle no datetime variables;
%LET datetime_vars = "";
PROC SQL NOPRINT;
	SELECT NAME
	INTO: datetime_vars SEPARATED BY " "
	FROM contents
	WHERE FORMAT = "DATETIME";
QUIT;

* If there are datetime variables, convert them to proper date format;
%IF &datetime_vars NE "" %THEN %DO;
	DATA tmp_data;
		SET tmp_data;
		FORMAT &datetime_vars E8601DZ.;
	RUN;
%END;

* Replace linebreaks with spaces in values to not mess up CSV export;
DATA tmp_data;
	SET tmp_data;
	ARRAY c _CHARACTER_;
  	DO OVER c;
    	c = TRANSLATE(c, "  ", "0A0D"x);
  	END;
RUN;

* Export compressed or uncompressed CSV;
%if &csv_file_ext = gz %THEN %DO;
	filename csv_fref ZIP "&out_file" ENCODING="utf-8" GZIP;
%END;
%ELSE %DO;
	filename csv_fref "&out_file" ENCODING="utf-8";
%END;

* Export to CSV file;
PROC EXPORT DATA=tmp_data
  OUTFILE=csv_fref
  DBMS=CSV REPLACE;
RUN;
