"""Convert all SAS file to CSV using sas_to_csv.sas in the shell."""

import asyncio
from asyncio.locks import Semaphore
from asyncio.subprocess import PIPE, STDOUT, Process
from pathlib import Path
from typing import Callable, Coroutine

SAS_PATH = Path("data-sas")
CSV_PATH = Path("data-csv")
SAS_SCRIPT = Path("sas_to_csv.sas")

sas_shell_cmd: Callable[[str | Path, str | Path], str] = (
    f'sas {SAS_SCRIPT} -nosplash -icon -log NUL -set in "{{}}" -set out "{{}}"'
).format


async def sas_to_csv(sas_file: Path, csv_file: Path) -> int:
    """Convert SAS data to CSV using sas_to_csv.sas in the shell."""
    print(f"[S]: {sas_file.name} > {csv_file.name}")

    process: Process = await asyncio.create_subprocess_shell(
        sas_shell_cmd(sas_file, csv_file), stdin=PIPE, stdout=PIPE, stderr=STDOUT
    )

    exit_code: int = await process.wait()

    if exit_code:
        print(f"[X]: {sas_file.name} > {csv_file.name}")
    else:
        print(f"[F]: {sas_file.name} > {csv_file.name}")

    return exit_code


async def convert_all_files(
    n: int, sas_files: list[Path], csv_files: list[Path]
) -> list[int]:
    """Convert all SAS files asynchronously, limiting to `n` parallel processes"""
    tasks: list[Coroutine] = [
        sas_to_csv(sas, csv) for sas, csv in zip(sas_files, csv_files)
    ]

    semaphore: Semaphore = Semaphore(n)

    async def sem_task(task: Coroutine) -> int:
        async with semaphore:
            return await task

    return await asyncio.gather(*(sem_task(task) for task in tasks))


def main():
    """Find and convert all SAS files into CSV."""
    get_csv_path: Callable[[Path], Path] = lambda sas_file: (
        CSV_PATH / sas_file.relative_to(SAS_PATH).with_suffix(".csv.gz")
    )

    # Find all SAS data files in the SAS data folder
    sas_files: list[Path] = list(SAS_PATH.glob("**/*.sas7bdat"))
    csv_files: list[Path] = [get_csv_path(f) for f in sas_files]

    # Create necessary folder structure
    for f in csv_files:
        f.parent.mkdir(parents=True, exist_ok=True)

    # Construct path to csv file based on SAS data file name
    exit_codes: list[int] = asyncio.run(convert_all_files(8, sas_files, csv_files))
    exit_codes = [bool(e) for e in exit_codes]

    print(
        f"[Finished]: Converted {len(exit_codes)-sum(exit_codes)}/{len(exit_codes)} "
        f"files, there were {sum(exit_codes)} errors."
    )


if __name__ == "__main__":
    main()
